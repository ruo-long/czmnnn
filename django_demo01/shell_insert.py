import os
import django

# 初始化django配置，注意修改blog_manager为项目名
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "blog_manager.settings")
django.setup()

# 以下代码导入模型，根据情况修改
from post.models import *

# 使用pycharm 调用 django 的 版本函数
print(django.get_version())

