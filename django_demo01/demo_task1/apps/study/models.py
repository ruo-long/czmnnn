from django.db import models


class Student(models.Model):
    sno = models.CharField(help_text="学号", max_length=32, unique=True)
    sname = models.CharField(help_text="姓名", max_length=32)
    sage = models.IntegerField(help_text="年龄", null=True)
    clazz_sid = models.CharField(help_text="班级编号", max_length=10)

    class Meta:
        db_table = "study_student"


class Clazz(models.Model):
    cname = models.CharField(help_text="班级名称", max_length=32)
    student = models.ManyToManyField(to=Student)

    class Meta:
        db_table = "study_clazz"
