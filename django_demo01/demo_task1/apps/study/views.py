from django.shortcuts import render
# from .models import Student, Clazz
from django.views.generic import View
from apps.study.models import Student, Clazz


class Studentinfo(View):
    def get(self, request):
        # 创建数据 第一种方法
        Student.objects.create(
            sno='209030322',
            sname="czm",
            sage=21,
            clazz_id='202',
        )
         # 创建数据 第二种方法
        student = Student(sno='209030301', sname="cwl", sage=21, clazz_id='201')
        student.save()
        # 创建数据 第三种方法
        # user = Modelsuser()
        # user.username = 'matktt'
        # user.age = 99
        # user.phone=112
        # user.create_time = '2022-10-21'
        # user.save()
        return render(request, 'student.html')


class Clazzinfo(View):
    def get(self, request):
        # 创建数据 第一种方法
        Clazz.objects.create(
            cname="python"
        )
