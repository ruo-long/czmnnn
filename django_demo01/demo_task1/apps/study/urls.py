from django.urls import path
from .views import Studentinfo, Clazzinfo
urlpatterns = [
    path('studentinfo/', Studentinfo.as_view()),
    path('clazzinfo/', Clazzinfo.as_view())
]
