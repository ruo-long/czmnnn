from django.urls import path
from apps.blog import views
from django.contrib import admin

urlpatterns = [
    path('article/index/', views.blog_index, name='article_index'),
    path('article/index/<int:page_index>/', views.blog_index, name='article_page_index'),
    # path('article/detail/<int:id>/', views.blog_detail, name='article_detail'),
    path('article_detail_view/<int:pk>/', views.ArticleDetailView.as_view(template_name="article_detail.html")),
    path('', views.Blog_Index.as_view(), name="index"),
    path('article/detail/<int:pk>/', views.Blog_Detail.as_view(), name="detail"),
    path('signup/', views.register, name="signup"),
    path('signin/', views.LoginView.as_view(), name="signin"),
    path('quit/', views.quit, name="quit"),
]
