from django import forms
from .models import *
from django.contrib.auth.models import User


class RegisterForm(forms.ModelForm):
    email = forms.EmailField(label="邮箱", required=True, error_messages={"requires": "请输入邮箱"})
    email_code = forms.CharField(max_length=4, label="邮箱验证码", error_messages={"required": "请输入邮箱验证码"})

    class Meta:
        model = User
        exclude = ("last_login", "date_joined")
        error_messages = {
            "username": {"required": "昵称必须输入"},
            "password": {"required": "请输入密码"},
            "email": {"required": "请输入有效的邮箱"}
        }


class LoginForm(forms.Form):
    username = forms.CharField(label="昵称", error_messages={"required": "请输入邮箱或昵称"})
    password = forms.CharField(label="密码", error_messages={"required": "请输入密码"})
