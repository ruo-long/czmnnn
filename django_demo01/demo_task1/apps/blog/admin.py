from django.contrib import admin
from django.db.models import F

from apps.blog.models import *


# ------blog--------
@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['category', 'title', 'author', 'abstract', "user", 'c_time']
    list_display_links = ['title']
    search_fields = ['title__contains', 'category__exact']
    fieldsets = (
        ('基本情况', {
            'fields': ['title', ('author', 'category'), 'abstract', 'content']
        }),
        ('交互信息', {
            # 折叠可以增加个classes
            'fields': [('like_num', 'against_num'), ('reply_num', 'read_num')]
        })
    )

    readonly_fields = ['like_num', 'against_num', 'reply_num', 'read_num']
    list_per_page = 5
    list_max_show_all = 50

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(LogInfo)
class LogInfoAdmin(admin.ModelAdmin):
    list_display = ['article', 'types', 'user', 'c_time']
    list_display_links = ['article']
    search_fields = ['article__contains']
    list_per_page = 5
    list_max_show_all = 50

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        t = obj.types
        if t == 0:
            Article.objects.filter(pk=obj.article.id).update(read_num=F('read_num') + 1)
        elif t == 1:
            Article.objects.filter(pk=obj.article.id).update(like_num=F('like_num') + 1)
        else:
            against_num = Article.objects.filter(pk=obj.article.id).values('against_num')[0].get('against_num')
            if against_num is None:
                Article.objects.filter(pk=obj.article.id).update(against_num=1)
            else:
                Article.objects.filter(pk=obj.article.id).update(against_num=F('against_num') + 1)
        super(LogInfoAdmin, self).save_model(request, obj, form, change)


@admin.register(CommentInfo)
class CommentInfoAdmin(admin.ModelAdmin):
    list_display = ['article', 'content', 'user', 'c_time']
    list_display_links = ['article']
    list_per_page = 5
    list_max_show_all = 50
    search_fields = ['article__contains', 'content__contains']
    ordering = ['-c_time', 'article']
    raw_id_fields = ["article"]

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        if not change:
            obj.user = request.user
            Article.objects.filter(pk=obj.article.id).update(reply_num=F("reply_num") + 1)
        super(CommentInfoAdmin, self).save_model(request, obj, form, change)
