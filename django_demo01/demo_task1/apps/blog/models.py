from django.db import models
from django.contrib.auth.models import User


class Article(models.Model):
    category = models.CharField(max_length=100, verbose_name="分类")
    title = models.CharField(max_length=100, verbose_name="标题")
    author = models.CharField(max_length=50, verbose_name="作者")
    abstract = models.CharField(max_length=500, verbose_name="文章摘要")
    content = models.TextField(verbose_name="文章内容")
    c_time = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")
    like_num = models.IntegerField(verbose_name="点赞数", default=0)
    reply_num = models.IntegerField(verbose_name="回复数", default=0)
    read_num = models.IntegerField(verbose_name="阅读数", default=0)
    against_num = models.IntegerField(verbose_name="反对数", null=True)
    user = models.ForeignKey(to=User, to_field="id", on_delete=models.CASCADE, verbose_name="操作员", null=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ("-c_time",)
        verbose_name_plural = "文章发布"


class LogInfo(models.Model):
    types_dict_choice = [(0, "阅读"), (1, "点赞"), (2, "拍砖")]
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    types = models.IntegerField(verbose_name="分类", choices=types_dict_choice, default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="操作员")
    c_time = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")

    class Meta:
        verbose_name_plural = "点评信息"


class CommentInfo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="操作员")
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    c_time = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")
    content = models.TextField(verbose_name="文章内容")

    def __str__(self):
        return self.content[:10]

    class Meta:
        ordering = ("-c_time",)
        verbose_name_plural = "文章评论"
