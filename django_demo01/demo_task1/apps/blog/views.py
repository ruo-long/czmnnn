from django.contrib import auth
from django.contrib.auth.hashers import make_password
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import DetailView, ListView
from django.views.generic.base import View
from .forms import *
from apps.blog.models import Article, CommentInfo
from demo_task1 import settings
from django.urls import reverse
from demo_task1 import urls


def article_info(article):
    comment_list = []
    comments = CommentInfo.objects.filter(article_id=article.id)
    for comment in comments:
        comment_list.append(comment.content)
    if len(comment_list) == 0:
        comment_list = ["该话题目前还没有被评论！"]
    return {
        'id': article.id,
        'title': article.title,
        'author': article.author,
        'abstract': article.abstract,
        'reply_num': article.reply_num,
        'read_num': article.read_num,
        'like_num': article.like_num,
        'content': str(comment_list),
    }


def blog_index(request, page_index=1):
    page_size = settings.PAGE_SIZE
    start_index = (page_index - 1) * page_size
    article_qs = Article.objects.all()[start_index:start_index + page_size]
    if len(article_qs) > 0:
        result_dict = {
            'status': 200,
            'page_index': page_index,
            'data': [article_info(article) for article in article_qs]
        }
    else:
        result_dict = {'status': 202}
    return JsonResponse(result_dict, json_dumps_params={'ensure_ascii': False})


def blog_detail(request, id):
    article = Article.objects.get(id=id)
    article_dict = article_info(article)
    return JsonResponse(article_dict, json_dumps_params={'ensure_ascii': False})


# 文章详情展示
class ArticleDetailView(DetailView):
    model = Article


class Blog_Index(ListView):
    model = Article
    template_name = "blog_index.html"
    queryset = Article.objects.order_by("-id")
    context_object_name = "article_list"
    paginate_by = 5


class Blog_Detail(DetailView):
    model = Article
    template_name = "blog_detail.html"
    context_object_name = "ar"


#  blog注册
def register(request):
    if request.method == "GET":
        return render(request, "blog_sign_up.html")
    if request.method == "POST":
        forms = RegisterForm(request.POST)
        if forms.is_valid():
            register = forms.save(commit=False)
            register.is_active = 1
            register.password = make_password(forms.cleaned_data['password'])
            register.save()
            # request.session['username'] = forms.cleaned_data['username']
            return redirect(reverse("signin"))
        else:
            print(forms.errors)
            return render(request, "blog_sign_up.html", {"forms": forms})


#  blog登录
class LoginView(View):
    def get(self, request):
        return render(request, "blog_sign_in.html")

    def post(self, requset):
        loginForm = LoginForm(requset.POST)
        if not loginForm.is_valid():
            return render(requset, "blog_sign_in.html", {"forms": loginForm})
        user_name = loginForm.cleaned_data["username"]
        password = loginForm.cleaned_data["password"]
        if user_name.find("@") > 0:
            user_obj = User.objects.filter(email=user_name)
            if len(user_obj) > 0:
                user_name = user_obj[0].username
        user = auth.authenticate(username=user_name, password=password)
        if user:
            requset.session["username"] = user_name
            return redirect(reverse("index"))
        else:
            return render(requset, "blog_sign_in.html", {"error": "用户名或密码错误"})


# blog退出
def quit(request):
    del request.session["username"]
    return redirect(reverse("index"))
