from django.contrib import auth
from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.views import View
from django.views.generic import ListView, DetailView
from .forms import *

from apps.post.models import Topic


def hello_bbs(request):
    html = "<h3>hello django bbs</h3>"
    return HttpResponse(html)


def welcome(request):
    return render(request, "welcome.html")


def defeat(request):
    return render(request, "defeat.html")


@csrf_exempt
def login_bbs(request):
    if request.method == 'GET':
        context = {}
        context['form'] = LoginForm
        return render(request, "login.html", context)
    elif request.method == 'POST':
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            return HttpResponseRedirect("../welcome")
        else:
            return render(request, "defeat.html", {"form": login_form})


def dynamic_bbs(request, year, month, day):
    return HttpResponse(f"({year}-{month}-{day})")


# 话题列表
class TopicListView(ListView):
    model = Topic


class LoginView(View):
    def get(self, requset):
        return render(requset, "login.html")

    def post(self, request):
        user_name = request.POST.get("username")
        pwd = request.POST.get("pwd")
        user = auth.authenticate(username=user_name, password=pwd)
        if user:
            auth.login(request, user)
            return redirect("../welcome")
        return HttpResponse("用户名或密码错误!")


def topic_add(request):
    if request.method == "GET":
        print(TopicForm)
        return render(request, 'topic_add.html', {'form': TopicForm()})
    if request.method == "POST":
        topic_form = TopicForm(request.POST)
        if topic_form.is_valid():
            topic_form.save()
            return redirect('topic_list_view')
        else:
            return render(request, "topic_add.html", {'form': topic_form})
