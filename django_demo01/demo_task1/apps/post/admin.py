from datetime import date
from django.contrib import admin
from ..post.models import *


# 注册model的方法二
@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ["title", "is_online", "content", "user", "created_time", "last_modified", "email"]
    list_display_links = ['content']
    list_filter = ['is_online', "title"]
    fieldsets = (
        ('基本情况', {
            'fields': ('title', 'is_online', 'content', 'user', "content_len")
        }),
        ('投票信息', {'classes': ('collapse', 'wide'),
                  'fields': ("created_time", "last_modified")
                  })
    )

    def content_len(self, obj):
        if obj.content is None:
            return 1
        else:
            return len(obj.content)

    def get_queryset(self, request):
        # today = date.today()
        author = request.user
        return self.model._default_manager.filter(user=author)

    # def has_delete_permission(self, request, obj=None):
    #     return False
    #
    # def has_add_permission(self, request):
    #     return False

    content_len.short_description = u'话题内容长度'
    readonly_fields = ["created_time", "last_modified", "content_len"]
    raw_id_fields = ["user"]
    actions = ["topic_online", "topic_offline"]

    def topic_online(self, request, queryset):
        rows_updated = queryset.update(is_online=True)
        self.message_user(request, "%s topics online" % rows_updated)

    topic_online.short_description = u"上线所选的 %s" % Topic._meta.verbose_name

    def topic_offline(self, request, queryset):
        rows_updated = queryset.update(is_online=False)
        self.message_user(request, "%s topics online" % rows_updated)

    topic_offline.short_description = u"下线所选的 %s" % Topic._meta.verbose_name

    def save_model(self, request, obj, form, change):
        pass

    def email(self, obj):
        return obj.user.email if obj.user != None else " "

    email.short_description = "邮箱"


# 注册model的方法一
# admin.site.register(Topic, TopicAdmin)
class CommentAdmin(admin.ModelAdmin):
    list_display = ["topic", "up", "down", "content", "created_time", "last_modified", "user"]
    search_fields = ["content__contains", "topic__title__startswith", "user__username__iexact"]  # 查询方式
    list_display_links = ["content"]  # 链接查询
    fieldsets = (
        ('基本情况', {
            'fields': ('topic', 'content', 'user')
        }),
        ('投票信息', {'classes': ('collapse', 'wide'),
                  'fields': ('up', 'down')
                  })
    )


admin.site.register(Comment, CommentAdmin)
