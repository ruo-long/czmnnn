from django.apps import AppConfig


class PostConfig(AppConfig):
    name = 'apps.post'
    verbose_name = "我的BBS"
