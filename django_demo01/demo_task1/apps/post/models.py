from django.db import models
from django.contrib.auth.models import User


class BaseModel(models.Model):
    created_time = models.DateTimeField(auto_now_add=True, help_text=u'创建时间', verbose_name="创建时间")
    last_modified = models.DateTimeField(auto_now=True, help_text=u'修改时间', verbose_name="最后修改时间")

    class Meta:
        abstract = True


class Topic(BaseModel):
    title = models.CharField(max_length=255, unique=True, help_text='话题标题', verbose_name="话题标题")
    content = models.TextField(help_text=u'话题内容', null=True, verbose_name="话题内容")
    is_online = models.BooleanField(help_text="是否在线", verbose_name="是否在线")
    user = models.ForeignKey(to=User, to_field='id', on_delete=models.CASCADE, null=True, verbose_name="发布者")

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'bbs_topic'
        verbose_name_plural = "话题"  # 话题复数
        ordering = ["-last_modified", "-created_time"]


class Comment(BaseModel):
    up = models.IntegerField(help_text="支持", default=0, verbose_name="支持")
    down = models.IntegerField(help_text="不支持", default=0, verbose_name="不支持")
    content = models.CharField(help_text="评论内容", max_length=255)
    topic = models.ForeignKey(to=Topic, to_field="id", on_delete=models.CASCADE, verbose_name="话题")
    user = models.ForeignKey(verbose_name='发布者', to=User, to_field='id', on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'bbs_comment'
        verbose_name_plural = "评论"
        ordering = ["-last_modified", "-created_time"]


class Role(models.Model):
    name = models.CharField("角色名称", max_length=50)
    user = models.ManyToManyField(to=User)


