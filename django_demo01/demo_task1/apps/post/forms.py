from django import forms
from .models import *


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()

    def clean_username(self):
        user_name = self.cleaned_data['username']
        check = User.objects.filter(username=user_name).exists()
        if not check:
            self.add_error('username', '该用户名不存在')
        return user_name


class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = '__all__'

    def clean_content(self):
        contents = self.cleaned_data['content']
        if "躺平" in contents or "摆烂" in contents:
            self.add_error('content', '存在敏感词汇')
        return contents