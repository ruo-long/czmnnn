from django.urls import path
from apps.post import views

urlpatterns = [
    path('hello/', views.hello_bbs),
    path('login/', views.login_bbs, name="login"),
    path('dynamic/<int:year>/<int:month>/<int:day>/', views.dynamic_bbs),
    path('welcome/', views.welcome),
    path('defeat/', views.defeat),
    # path('topic_list/<int:page>/', views.topic_list),
    path('topic_list_view/', views.TopicListView.as_view(template_name="test01.html")),
    path('loginview/', views.LoginView.as_view()),
    path('topic_add/', views.topic_add, name="topic_add"),

]
