# czmnnn

#### 介绍
Django+DRF+html


#### docker部署该项目
1、在云服务上git clone https://gitee.com/ruo-long/czmnnn.git获取该项目
项目部署后入下图，只保存demo_task1，其为Django服务

![输入图片说明](https://foruda.gitee.com/images/1685524118662853927/c8e91fa5_11860896.png "屏幕截图")
2、cd demo_task1
在该目录下有Dockerfile和docker-compose.yml
Dockerfile配置如下（内容无需修改）：
FROM python:3.7

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code

COPY requirements.txt /code/
RUN pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

COPY ./ /code/
docker-compose.yml配置如下：
version: '3'

services:
  db:
    image: mysql:5.7
    environment:
      MYSQL_DATABASE: 'bbs'
      MYSQL_USER: 'blog'
      MYSQL_PASSWORD: '123456'
      MYSQL_ROOT_PASSWORD: '123456'
    ports:
      - "3306:3306"
    volumes:
      - db_data:/var/lib/mysql
  web:
    image: django_blog:test
    build: .
    command: python manage.py runserver 0.0.0.0:8000
    ports:
      - "8379:8000"
    volumes:
      - .:/code
    depends_on:
      - db
volumes:
  db_data:
其中MYSQL_DATABASE和MYSQL_USER可以直接自定义，image: django_blog:test可以不要，这个一般是使用build后加上的，没的话后面的build .会构建一个，只是名字不一样。
#### 修改该项目中的setting
vi demo_task1/settings.py
![输入图片说明](https://foruda.gitee.com/images/1685524751512984705/9453609d_11860896.png "屏幕截图")
其中*表示所以ip，最好写自己云服务的那个ip
![输入图片说明](https://foruda.gitee.com/images/1685524888977908395/188a8ec6_11860896.png "屏幕截图")
数据库的配置修改如上
接下来就可以启动容器，使用docker-compose up -d 后台启动
![输入图片说明](https://foruda.gitee.com/images/1685525223772878080/9ccaa5d8_11860896.png "屏幕截图")

这个时候可能会出现decode的错误，是在op***.py的146，我们只需将其换成encode即可，但是其中需要进入到容器中
即 docker-compose exec web bash,然后使用apt-get update 和 apt-get install vim 这两个命令安装vi就可以编辑了
在然后要执行 python manage.py makemigrations 和 python manage.py migrate进行数据库的迁移。
最后就可以正常的访问项目了
![输入图片说明](https://foruda.gitee.com/images/1685525533037222567/a0c3fafe_11860896.png "屏幕截图")